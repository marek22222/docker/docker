package main

import (
	"log"

	"github.com/streadway/amqp"
)

func blad(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	blad(err, "Błąd połączenia do RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	blad(err, "Błąd otwarcia kanału")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"logs",   // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	blad(err, "Błąd deklaracji exchange'a")

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	blad(err, "Błąd deklaracji kolejki")

	err = ch.QueueBind(
		q.Name, // queue name
		"",     // routing key
		"logs", // exchange
		false,
		nil,
	)
	blad(err, "Błąd bindowania kolejki")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	blad(err, "Błąd rejestracji konsumenta")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf(" [x] %s", d.Body)
		}
	}()

	log.Printf(" [*] Oczekiwanie na wiadomości. Aby zakończyć naciśnij CTRL+C")
	<-forever
}
