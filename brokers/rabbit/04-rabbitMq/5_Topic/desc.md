In one terminal:
    go run receive/receive_logs_direct.go warning error > logs_from_rabbit.log

In second terminal:
    go run receive_logs_topic.go info warning error
    go run receive_logs_topic.go info "Run. Run. Info."
    go run receive_logs_topic.go error "Run. Run. error."    
    go run receive_logs_topic.go warning "Run. Run. warning."



