Youshould run before rabbitMQ:
    docker start container_id

In one terminal:
    go run receive/receive_logs_direct.go warning error > logs_from_rabbit.log

In second terminal:
    go run emit_log_direct.go info warning error
    go run emit_log_direct.go info "Run. Run. Info."
    go run emit_log_direct.go error "Run. Run. error."    
    go run emit_log_direct.go warning "Run. Run. warning."



