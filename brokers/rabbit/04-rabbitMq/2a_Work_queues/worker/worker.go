package main

import (
	"bytes"
	"log"
	"time"

	"github.com/streadway/amqp"
)

func blad(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	blad(err, "Błąd połączenia do RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	blad(err, "Błąd otwarcia kanału")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"hello", // name
		false,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	blad(err, "Błąd deklaracji kolejki")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	blad(err, "Błąd rejestracji konsumenta")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Wysłano wiadomość: %s", d.Body)
			dotCount := bytes.Count(d.Body, []byte("."))
			t := time.Duration(dotCount)
			time.Sleep(t * time.Second)
			log.Printf("Poszło")
		}
	}()

	log.Printf(" [*] Oczekiwanie na wiadomości. Aby zakończyć naciśnij CTRL+C")
	<-forever
}
