Get Started with RabbitMQ on Docker
    How to quickly spin up RabbitMQ instances with Docker and Docker Compose.
 by Changhui Xu

Based on https://codeburst.io/get-started-with-rabbitmq-on-docker-4428d7f6e46b



## RabbitMQ Setup with Docker

In your terminal, run
    docker run --rm -it --hostname my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3.11-management-alpine

This will create a docker container with a rabbitMQ instance.

In your browser, navigate to localhost:15672, you should see the RabbitMQ UI; login with guest:guest as the username and password.


## Connect and Publish Messages with RabbitMQ

go get -u GitHub.com/streadway/amqp




