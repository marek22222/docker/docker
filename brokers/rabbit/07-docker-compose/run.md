How to Set Up RabbitMQ With Docker Compose
Based on https://fullstackcode.dev/2022/08/13/setup-rabbitmq-with-docker/


# Open a terminal, navigate to your rabbitmq-go folder and run 
./start


Once you see this, open your browser and head over to
http://localhost:15672. 
You should see the RabbitMQ UI. Use guest as username and password.


# Publishing a Message With Go
Now that RabbitMQ is ready to Go, let's connect to it and send a message to the queue. But first, we need the amqp library. To install it, run the following command in your terminal: go get github.com/streadway/amqp.

Once we've created the main.go file, let's test sendMessage with the following terminal command: 
    go run main.go  // in current dir 

You should see something like this:
    go run main.go 
    What message do you want to send?
    aaaaaaaa
    [x] Congrats, sending message: aaaaaaaa


# Reading Messages With Go

Create a file called main.go inside consumer directory.

Let's test consumer/main.go with the following terminal command:
    go run consumer/main.go 
    [*] Waiting for messages. To exit press CTRL+C
    Received a message: aaaaaaaa

