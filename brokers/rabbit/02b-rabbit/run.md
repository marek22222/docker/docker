Go RabbitMQ Beginners Tutorial
    by Elliot Forbes

Based on https://tutorialedge.net/golang/go-rabbitmq-tutorial/


## RabbitMQ Setup with Docker

docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

In your browser, navigate to localhost:15672, you should see the RabbitMQ UI; login with guest as the username and password.


## Consuming Messages From a Queue
Start by creating a new file called in which we’ll add the following: 'consumer.go'.

Producent:
PS C:\Users\user\go\src\gitlab.com\marek2222\docker\docker\brokers\rabbit\rabbit-02b> cd .\producer\
PS C:\Users\user\go\src\gitlab.com\marek2222\docker\docker\brokers\rabbit\rabbit-02b\producer> go run .\main.go
Go RabbitMQ Tutorial
{TestQueue 0 1}
Successfully Published Message to Queue

Consumer:
PS C:\Users\mmusial\go\src\gitlab.com\marek2222\docker\docker\brokers\rabbit\rabbit-02b\consumer> go run .\consumer.go
Go RabbitMQ Tutorial
Successfully Connected to our RabbitMQ Instance
 [*] - Waiting for messages
Recieved Message: Hello World
Recieved Message: Hello World
Recieved Message: Hello World
Recieved Message: Hello World




