
Tworzenie klastra
https://kubernetes.io/pl/docs/tutorials/kubernetes-basics/create-cluster/


## Module 1 - Create a Kubernetes cluster
# 1. Cluster up and running

minikube version  
    1.18.0

minikube start
    Great! You now have a running Kubernetes cluster in your online terminal. Minikube started a virtual machine for you, and a Kubernetes cluster is now running in that VM.


# 2. Cluster up and running

kubectl version


# 3. Cluster details

kubectl cluster-info
    Kubernetes control plane is running at https://192.168.59.101:8443
    CoreDNS is running at https://192.168.59.101:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

kubectl get nodes
    This command shows all nodes that can be used to host our applications. Now we have only one node, and we can see that its status is ready (it is ready to accept applications for deployment).
