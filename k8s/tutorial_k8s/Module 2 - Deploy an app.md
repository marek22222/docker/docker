
Tworzenie klastra
https://kubernetes.io/pl/docs/tutorials/kubernetes-basics/create-cluster/


## Module 2 - Deploy an app

    The goal of this scenario is to help you deploy your first app on Kubernetes using kubectl. You will learn the basics about kubectl cli and how to interact with your application.

# 1. kubectl basics

kubectl version  
    Client Version: v1.25.1
    Kustomize Version: v4.5.7
    Server Version: v1.24.1
    OK, kubectl is installed and you can see both the client and the server versions.


kubectl get nodes
    To view the nodes in the cluster, run the kubectl get nodes command
    Here we see the available nodes (1 in our case). Kubernetes will choose where to deploy our application based on Node available resources.



# 2. Deploy our app

    Let’s deploy our first app on Kubernetes with the kubectl create deployment command. We need to provide the deployment name and app image location (include the full repository url for images hosted outside Docker hub).

kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
    deployment.apps/kubernetes-bootcamp created

    Great! You just deployed your first application by creating a deployment. This performed a few things for you:

    - searched for a suitable node where an instance of the application could be run (we have only 1 available node)
    - scheduled the application to run on that Node
    - configured the cluster to reschedule the instance on a new Node when needed


    To list your deployments use the get deployments command:
kubectl get deployments
    NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
    kubernetes-bootcamp   0/1     1            0           11m

    We see that there is 1 deployment running a single instance of your app. The instance is running inside a Docker container on your node.

# 3. View our app
    Pods that are running inside Kubernetes are running on a private, isolated network. By default they are visible from other pods and services within the same kubernetes cluster, but not outside that network. When we use kubectl, we're interacting through an API endpoint to communicate with our application.

    We will cover other options on how to expose your application outside the kubernetes cluster in Module 4.

    The kubectl command can create a proxy that will forward communications into the cluster-wide, private network. The proxy can be terminated by pressing control-C and won't show any output while its running.

    We will open a second terminal window to run the proxy.

echo -e "\n\n\n\e[92mStarting Proxy. After starting it will not output a response. 
Please click the first Terminal Tab\n"; 
kubectl proxy

    We now have a connection between our host (the online terminal) and the Kubernetes cluster. The proxy enables direct access to the API from these terminals.

    You can see all those APIs hosted through the proxy endpoint. For example, we can query the version directly through the API using the curl command:

curl http://localhost:8001/version

    Note: Check the top of the terminal. The proxy was run in a new tab (Terminal 2), and the recent commands were executed the original tab (Terminal 1). The proxy still runs in the second tab, and this allowed our curl command to work using localhost:8001.

    If Port 8001 is not accessible, ensure that the kubectl proxy started above is running.

    The API server will automatically create an endpoint for each pod, based on the pod name, that is also accessible through the proxy.

    First we need to get the Pod name, and we'll store in the environment variable POD_NAME:

export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME

    You can access the Pod through the API by running:

curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/

    In order for the new deployment to be accessible without using the Proxy, a Service is required which will be explained in the next modules.

