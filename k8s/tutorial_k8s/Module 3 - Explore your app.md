
Tworzenie klastra
https://kubernetes.io/pl/docs/tutorials/kubernetes-basics/create-cluster/


## Module 3 - Explore Your App
# Viewing Pods and Nodes

    Rozwiązywanie problemów przy pomocy kubectl
    W module 2 używałeś narzędzia Kubectl. W module 3 będziemy go nadal używać, aby wydobyć informacje na temat zainstalowanych aplikacji i środowiska, w jakim działają. Najczęstsze operacje przeprowadzane są przy pomocy następujących poleceń kubectl:

kubectl get         - wyświetl informacje o zasobach
kubectl describe    - pokaż szczegółowe informacje na temat konkretnego zasobu
kubectl logs        - wyświetl logi z kontenera w danym podzie
kubectl exec        - wykonaj komendę wewnątrz kontenera w danym podzie
    
    Korzystaj z tych poleceń, aby sprawdzić, kiedy aplikacja została zainstalowana, jaki jest jej aktualny status, gdzie jest uruchomiona i w jakiej konfiguracji.

    Kiedy już wiemy więcej na temat części składowych klastra i podstawowych poleceń, przyjrzyjmy się naszej aplikacji.


# Step 1 Check application configuration

    Let’s verify that the application we deployed in the previous scenario is running. We’ll use the kubectl get command and look for existing Pods:

kubectl get pods

    If no pods are running then it means the interactive environment is still reloading it's previous state. Please wait a couple of seconds and list the Pods again. You can continue once you see the one Pod running.

    Next, to view what containers are inside that Pod and what images are used to build those containers we run the describe pods command:

kubectl describe pods

    We see here details about the Pod’s container: IP address, the ports used and a list of events related to the lifecycle of the Pod.

    The output of the describe command is extensive and covers some concepts that we didn’t explain yet, but don’t worry, they will become familiar by the end of this bootcamp.

    Note: the describe command can be used to get detailed information about most of the kubernetes primitives: node, pods, deployments. The describe output is designed to be human readable, not to be scripted against.


# Step 2 Show the app in the terminal
    Recall that Pods are running in an isolated, private network - so we need to proxy access to them so we can debug and interact with them. To do this, we'll use the kubectl proxy command to run a proxy in a second terminal window. Click on the command below to automatically open a new terminal and run the proxy:

echo -e "\n\n\n\e[92mStarting Proxy. After starting it will not output a response. Please click the first Terminal Tab\n"; kubectl proxy

    Now again, we'll get the Pod name and query that pod directly through the proxy. To get the Pod name and store it in the POD_NAME environment variable:

export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
echo Name of the Pod: $POD_NAME

    To see the output of our application, run a curl request.

curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/

    The url is the route to the API of the Pod.