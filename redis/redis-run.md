How To Deploy And Run Redis In Docker 
    by Vladimir Kaplarevic
Based on https://phoenixnap.com/kb/docker-redis

# Start a Docker Redis Container
1. Check the current status of the Docker service by entering the following command in your terminal:
```
    sudo systemctl status docker
```
2. Retrieve and start a Redis container (my-first-redis) with the docker run command:
```
    sudo docker run --name my-first-redis -d redis
```


Connect to Redis with redis-cli
```
    sudo docker exec -it myRedis sh
```

Once you access the interactive shell, type 
```
    redis-cli 
```
to connect to the Redis container instance.

Type to authenticate
```
    redis-server --requirepass yourpassword
```

## Try Basic Redis Commands

1. The Redis ping command is useful for testing if a connection to the Redis database is active:
```
    ping
```
The response, PONG, indicates that the connection is successful.

2. Key-value stores use the simplest possible data model. A unique key is paired with a value. Use the set command to define the key name and the value pair as pnap:
```
    set name pnap
```
Basic Redis commands using redis-cli.

3. You can retrieve the value using the unique key name and the get command:
```
    get name
```
The result retrieves the previously defined pnap value. A list of data types and commands is available in our comprehensive guide Redis Data Types With Commands.

4. Once you have explored redis-cli commands, type quit to return to the container terminal interface.

5. Type exit to close the connection with the Docker container.




# Using a Custom redis.conf File (Optional)
The redis.conf file allows you to set up authentication, limit commands, and define other security-oriented settings.

If you have created a custom Redis configuration file, use the following command to load the file at container launch:
```
    sudo docker run --name my-first-redis -v /myfirstredis/redis.conf:/usr/local/etc/redis/redis.conf -d redis
```
The location of the redis.conf file in this example is myfirstredis/redis.conf. Change the path to match the location on your system.

# Access Redis from Another Docker Container
1. Use the --link option to create and connect a new container to the existing Redis instance:
```
    sudo docker run -it --rm --name my-second-redis --link my-first-redis:redis -d redis
```
The command initiated a new Redis container (my-second-redis), based on the initial redis image. The my-first-redis container is going to be referred to as redis within the second container.

Linking Redis containers with docker run command.
Additionally, the --rm option ensures that the second container deletes itself after exiting the interactive shell. This action is optional and is used to conserve resources.

2. Initiate the interactive shell within the my-second-redis container:
```
    sudo docker exec -it my-second-redis sh
```
3. Start the Redis command-line in the my-second-redis container and connect to my-first-redis (now named redis), with the following command:
```
    # redis-cli -h redis
```

4. The original my-first-redis container is still active. The name key created earlier is available and can be accessed by the second container:
```
    redis:6379> get name
```

The resulting pair value is pnap.

Key-pair value shared between linked Redis containers.
5. Enter quit to leave the redis-cli and then type exit to return to your terminal:
```
    redis:6379> quit
    # exit
```
Note: Read our guide to learn more about sharing data between Docker containers.


# Access Redis from Remote Server
You can use the Docker port-forwarding function to access Redis containers from remote servers.

1. Define the port to be used for the remote connection:
```
    sudo docker run --name my-first-redis -p [port_number]:6379 -d redis
```

2. Access the Redis container from a remote server using the host-name or IP and the newly defined port number:
```
    sudo redis-cli -h [host or IP] -p [port_number] -a [password]
```
The -a authentication flag is optional. If used, it requests users to enter their password to access the Redis database.

Note: If you are working with Kubernetes, our Knowledge Base also has a guide on how to deploy a Redis Cluster on Kubernetes.


Conclusion

You have successfully deployed a Redis instance within a Docker container.

Use the commands presented in this tutorial to deploy Redis on other servers in your cluster, link the containers, and access Redis containers from remote servers.

Vladimir Kaplarevic